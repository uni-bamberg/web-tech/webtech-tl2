var greenSound, redSound, blueSound, yellowSound, errorSound;

var score = 0;
var highscore = 0;
var counter = 0;
var sequence = [];
var presentDelay = 1000;
var highlightDelay = 400;



$(document).ready(function () {

    loadSounds();
    $('.colorbutton').addClass('pressed');

    greenSound.play();

    // disable proceed button on startup
    $('#proceedButton').prop('disabled', true);

    // init new game on button click
	$('#newGameButton').on('click', function() {
		init();
	});

	// click handler for proceed button, get new colors, reset counter and disable button after click
	$('#proceedButton').on('click', function() {
		getNewSequence();
		counter = 0;
		$('#proceedButton').prop('disabled', true);
	});
});

/**
 * Click handler for colors
 *
 * Checks if clicked color is the correct color in line
 * if true -> gameOn
 * if false -> gameOver
 */

function colorButtonClickHandler() {
	clickedColor = $(this).prop('id');
	if (sequence[counter] === clickedColor) {
		gameOn(clickedColor);
	} else {
		gameOver();
	}
}

/**
 * Game over
 *
 * Plays game over sound, re-activates the new game button and adds the class "pressed" to colorbutton
 */

function gameOver() {

    $('#newGameButton').prop('disabled', false);

	errorSound.play();

    //reset sequence
    sequence = [];

    $('.colorbutton').addClass('pressed');
}

/**
 * Game on
 *
 * Highlights clicked color and update counter
 * Checks if end of sequence is reached
 * - updates score
 * - highscore if necessary
 * - activates proceed button and unbinds eventhandler
 *
 * @param color
 */

function gameOn(color) {
	highlightColor(color);
	counter++;

	if (counter === sequence.length) {
		score++;
		$('#score').html(score);
		$('.colorbutton').unbind();
		if (score > highscore) {
			highscore = score;
			$('#highscore').html(highscore);
		}

		$('#proceedButton').prop('disabled', false);
	}
}

/**
 * Init
 *
 * Resets game settings
 * Activates and disables buttons
 * Gets new color sequence 
 */

function init() {
	counter = 0;
	sequence = [];
	score = '0';
	$('#score').html(score);
	$('.colorbutton').removeClass('pressed');
	$('#newGameButton').prop('disabled', true);

	// get initial colors
	getNewSequence();
}

/**
 * Present sequence
 *
 * Highlight colors and plays sound of each color one by one
 *
 * @param sequence
 */

function presentSequence(sequence) {
	$(sequence).each(function(i, color) {
		setTimeout(function() {
			highlightColor(color);
		}, i * presentDelay);
	});
}

/**
 * Highlight color
 *
 * gets a color and adds the class pressed. After highlight delay the class will be removed
 *
 * @param color
 */

function highlightColor(color) {
	const colorButton = $('#'+color);
	colorButton.addClass('pressed');
	playSound(color);
	setTimeout(function () {
		colorButton.removeClass('pressed');
	}, highlightDelay);
}


function playSound(color) {
	switch (color) {
		case 'green':
			greenSound.play();
			break;
		case 'red':
			redSound.play();
			break;
		case 'blue':
			blueSound.play();
			break;
		case 'yellow':
			yellowSound.play();
			break;
		default:
			showError('Invalid color!');
	}
}

/**
 * Get new sequence
 *
 * Ajax request to get new ColorSequence from nextColor.php. 
 * Checks if result is valid.
 *
 */

function getNewSequence() {
	const colorButton = $('.colorbutton');
	colorButton.unbind('click', colorButtonClickHandler);

	$.ajax({
		url: "php/nextColor.php?sequence=" + JSON.stringify(sequence),
		success: function(result) {
			let jsonObject = {};
			if (result) {
				try {
					jsonObject = $.parseJSON(result);
				} catch (e) {
					showError(e + '\nResult: ' + result);
				}

				if (!jsonObject['status'] || jsonObject['status'] !== 'okay') {
					showError('Invalid status');
				}
				if (jsonObject['sequence'] && $.isArray(jsonObject['sequence'])) {
					sequence = jsonObject['sequence'];
					presentSequence(sequence);
				} else {
					showError('Invalid sequence');
				}
			} else {
				showError('No result');
			}
		},
		complete: function() {
			setTimeout(function() {
				colorButton.bind('click', colorButtonClickHandler);
			}, ((sequence.length-1) * presentDelay) + highlightDelay);
		},
		error: function(e){
			showError(e + '\n\nThere is an error with AJAX!');
		}
	});
}


/**
 * Show error
 *
 * Reload page after alert message
 *
 * @param message
 */

function showError(message) {
	alert(message);
	location.reload();
}

function loadSounds() {
    greenSound = new Howl({
        src: ['sounds/green.mp3','sounds/green.ogg']
    });

    redSound = new Howl({
        src: ['sounds/red.mp3','sounds/red.ogg']
    });

    blueSound = new Howl({
        src: ['sounds/blue.mp3','sounds/blue.ogg']
    });

    yellowSound = new Howl({
        src: ['sounds/yellow.mp3','sounds/yellow.ogg']
    });

    errorSound = new Howl({
        src: ['sounds/error.mp3','sounds/error.ogg']
    });
}

function playSound(color) {
	switch (color) {
		case 'green':
			greenSound.play();
			break;
		case 'red':
			redSound.play();
			break;
		case 'blue':
			blueSound.play();
			break;
		case 'yellow':
			yellowSound.play();
			break;
		default:
			showError('Invalid color!');
	}
}

//not used
function copyArray(input) {
    var copy = input.slice();
    return copgy;
}